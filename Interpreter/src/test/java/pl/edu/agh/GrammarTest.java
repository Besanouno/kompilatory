package pl.edu.agh;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.TokenStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.edu.agh.pl.edu.agh.exceptions.ExceptionThrowingErrorHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.LinkedList;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

@RunWith(Parameterized.class)
public class GrammarTest {

    private static final String SEPARATOR = "----";

    @Parameterized.Parameters
    public static Collection<Object[]> data() throws IOException {

        BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        GrammarTest.class.getResourceAsStream("/test.txt")));

        LinkedList<Object[]> objects = new LinkedList<>();


        String line;
        while ((line = br.readLine()) != null) {
            boolean correct = Boolean.parseBoolean(line);
            br.readLine(); // omits separator

            StringBuilder code = new StringBuilder();
            while (!(line = br.readLine()).equals(SEPARATOR)) {
                code.append(line);
            }

            objects.add(new Object [] {correct, code.toString()});
        }
        return objects;
    }

    private final boolean testValid;
    private final String testString;

    public GrammarTest(boolean testValid, String testString) {
        this.testValid = testValid;
        this.testString = testString;
    }

    @Test
    public void testRule() {
        ANTLRInputStream input = new ANTLRInputStream(this.testString);
        GrammarLexer lexer = new GrammarLexer(input);
        TokenStream tokens = new CommonTokenStream(lexer);

        GrammarParser parser = new GrammarParser(tokens);

        parser.removeErrorListeners();
        parser.setErrorHandler(new ExceptionThrowingErrorHandler());

        if (this.testValid) {
            ParserRuleContext ruleContext = parser.rules();
            assertNull(ruleContext.exception);
        } else {
            try {
                ParserRuleContext ruleContext = parser.rules();
                fail("Failed on \"" + this.testString + "\"");
            } catch (RuntimeException e) {
                // deliberately do nothing
            }
        }
    }
}