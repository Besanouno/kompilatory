grammar Grammar;

IF   : 'if' ;
THEN : 'then';
ELSE : 'else';
LOOP : 'while';
END : 'end';
VOID : 'void';

AND : 'and' ;
OR  : 'or' ;

TRUE  : 'true' ;
FALSE : 'false' ;

STRING : '"' .+? '"';
CHAR : '\'' . '\'';

MULT  : '*' ;
DIV   : '/' ;
PLUS  : '+' ;
MINUS : '-' ;

GT : '>'  ;
GE : '>=' ;
LT : '<'  ;
LE : '<=' ;
EQ : '==' ;
NEQ: '<>' ;

ASSIGN : '=';
RETURN : 'return';

LPAREN : '(' ;
RPAREN : ')' ;
BLOCK_START : '{';
BLOCK_END : '}';
SEMI : ';' ;
COLON : ':' ;
DOT : '.';
COMMA : ',';

NUMBER : '-'?[0-9]+('.'[0-9]+)? ;
IDENTIFIER : [a-zA-Z_][a-zA-Z_0-9]* ;

BLOCKCOMMENT : '/*' .+? '*/' -> skip;
COMMENT : '//' .+? ('\n'|EOF) -> skip ;
WS : [ \r\t\u000C\n]+ -> skip ;

rules :
    function* EOF ;

function :
    function_declaration
    BLOCK_START
    function_body
    BLOCK_END
;

function_declaration :
    return_type IDENTIFIER LPAREN (function_param (COMMA function_param)*)? RPAREN
;

function_param : type IDENTIFIER;

function_body:
    (var_declaration | statement) *
    ;

statement
    : statement_if
    | statement_loop
    | statement_assign
    | statement_return
    | function_call DOT
;

function_call : IDENTIFIER LPAREN (expr (COMMA expr)*)? RPAREN # FunctionCall;

statement_if : IF logical_expr THEN function_body statement_else* END # StatementIf;

statement_else : ELSE IF logical_expr THEN function_body;

statement_loop : LOOP logical_expr THEN function_body END # StatementLoop;

statement_assign : IDENTIFIER ASSIGN expr DOT # StatementAssign;

statement_return : RETURN expr DOT # StatementReturn;

logical_expr
 : function_call                 # LogicalExpressionFunctionCall
 | logical_expr AND logical_expr # LogicalExpressionAnd
 | logical_expr OR logical_expr  # LogicalExpressionOr
 | comparison_expr               # ComparisonExpression
 | LPAREN logical_expr RPAREN    # LogicalExpressionInParen
 | logical_entity                # LogicalEntity
 ;

comparison_expr : arithmetic_expr EQ arithmetic_expr # ComparisonExpressionEqual
                | arithmetic_expr NEQ arithmetic_expr # ComparisonExpressionNotEqual
                | arithmetic_expr GT arithmetic_expr # ComparisonExpressionGreaterThan
                | arithmetic_expr GE arithmetic_expr # ComparisonExpressionGreaterEqual
                | arithmetic_expr LT arithmetic_expr # ComparisonExpressionLowerThan
                | arithmetic_expr LE arithmetic_expr # ComparisonExpressionLowerEqual
                | var_entity EQ var_entity           # ComparisonExpressionVarEntity
                | LPAREN comparison_expr RPAREN        # ComparisonExpressionParens
                ;

arithmetic_expr
 : arithmetic_expr MULT arithmetic_expr  # ArithmeticExpressionMult
 | arithmetic_expr DIV arithmetic_expr   # ArithmeticExpressionDiv
 | arithmetic_expr PLUS arithmetic_expr  # ArithmeticExpressionPlus
 | arithmetic_expr MINUS arithmetic_expr # ArithmeticExpressionMinus
 | MINUS arithmetic_expr                 # ArithmeticExpressionNegation
 | LPAREN arithmetic_expr RPAREN         # ArithmeticExpressionParens
 | function_call                         # ArithmeticFunctionCall
 | NUMBER                                # ArithmeticExpressionNumberEntity
 | IDENTIFIER                            # ArithmeticExpressionIdentifierEntity
 ;

logical_entity : (TRUE | FALSE)       # LogicalConst
               | IDENTIFIER           # LogicalVariable
               ;

var_entity     : NUMBER               # NumericConst
               | IDENTIFIER           # Variable
               | STRING               # StringConst
               | CHAR                 # CharConst
               ;

var_declaration : type IDENTIFIER ( ASSIGN expr)? DOT # VariableDeclaration;

return_type : VOID | type;

type : number
        | character
        | string
        | bool
;

number : 'number';
character : 'char';
string : 'string';
bool : 'bool';

expr
 : var_entity           # ExpressionVarEntity
 | function_call        # ExpressionFunctionCall
 | arithmetic_expr      # ExpressionArithmetic
 | logical_expr         # ExpressionLogical
 | logical_entity       # ExpressionLogicalEntity
;