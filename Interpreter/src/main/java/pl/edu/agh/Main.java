package pl.edu.agh;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import pl.edu.agh.pl.edu.agh.exceptions.ExceptionThrowingErrorHandler;
import pl.edu.agh.visitors.InterpreterVisitor;
import pl.edu.agh.visitors.InvocationVisitor;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        InterpreterVisitor visitor = new InterpreterVisitor();
        String consoleInput;
        while(!(consoleInput = getInput(scanner)).equals("quit")) {
            System.out.println("Your input:\n" + consoleInput);

            try {
                ANTLRInputStream input = new ANTLRInputStream(consoleInput);
                GrammarLexer lexer = new GrammarLexer(input);
                CommonTokenStream tokens = new CommonTokenStream(lexer);
                GrammarParser parser = new GrammarParser(tokens);
                parser.setBuildParseTree(true);
                parser.removeErrorListeners();
                parser.setErrorHandler(new ExceptionThrowingErrorHandler());

                ParserRuleContext tree = parser.rules();
                //tree.inspect(parser); //shows ast in gui
                visitor.visit(tree);

                System.out.println("Success!\n");
            } catch (Exception e) {
                visitor.functions.remove("main");
                e.printStackTrace();
                System.out.println("Failure!\n" + e.getMessage());
            }
        }
    }

    private static String getInput(Scanner scanner) {

        StringBuilder stringBuilder = new StringBuilder();

        while (true) {
            String s = scanner.nextLine();
            if (s.equals("")) {
                continue;
            }

            if (s.charAt(s.length()-1) == '/') {
                stringBuilder.append(s.substring(0, s.length() - 1)).append("\n");
            } else {
                stringBuilder.append(s);
                break;
            }
        }
        return stringBuilder.append("\n").toString();
    }
}
