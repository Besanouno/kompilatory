package pl.edu.agh.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ReturnValue {
    private Object value;
}
