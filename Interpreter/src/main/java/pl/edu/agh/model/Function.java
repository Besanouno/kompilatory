package pl.edu.agh.model;


import lombok.*;
import org.antlr.v4.runtime.ParserRuleContext;
import pl.edu.agh.GrammarParser;
import pl.edu.agh.enums.Type;

import java.util.List;

@Builder
@Getter
public class Function {

    private List<Parameter> parameters;
    private String name;
    private Type returnType;
    private GrammarParser.Function_bodyContext bodyContext;

    @Getter
    @AllArgsConstructor
    public static class Parameter {
        private String name;
        private Type type;


        @Override
        public int hashCode() {
            return name.hashCode();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Parameter)) return false;

            Parameter that = (Parameter) o;

            if (!this.name.equals(that.getName()))
                return false;

            return true;
        }
    }

}
