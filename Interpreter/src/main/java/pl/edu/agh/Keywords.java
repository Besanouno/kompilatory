package pl.edu.agh;

import java.util.ArrayList;
import java.util.List;

public class Keywords {
    private static List<String> keywords = new ArrayList<String>() {
        {
            add("bool");
            add("number");
            add("string");
            add("char");
            add("if");
            add("else");
            add("then");
            add("end");
            add("while");
            add("true");
            add("false");
            add("and");
            add("or");
            add("void");
            add("return");
        }
    };

    public static boolean isKeyword(String word) {
        return keywords.contains(word);
    }
}
