package pl.edu.agh.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Type {

    BOOL("bool"), NUMBER("number"), CHAR("char"), STRING("string"), VOID("void");

    private String name;
}
