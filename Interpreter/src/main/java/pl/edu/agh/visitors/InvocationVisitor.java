package pl.edu.agh.visitors;

import lombok.Getter;
import org.antlr.v4.runtime.tree.ParseTree;
import pl.edu.agh.GrammarBaseVisitor;
import pl.edu.agh.GrammarParser;
import pl.edu.agh.enums.Type;
import pl.edu.agh.model.Function;
import pl.edu.agh.model.Parameter;
import pl.edu.agh.model.ReturnValue;

import java.util.*;

@Getter
public class InvocationVisitor extends GrammarBaseVisitor {

    private Map<String, Function> functions;
    private Map<String, Object> variables;
    private String functionName;
    private ReturnValue returnValue;


    public InvocationVisitor(Map<String, Function> functions, Map<String, Object> variables, String functionName) {
        this.functions = functions;
        this.variables = variables;
        this.functionName = functionName;
    }

    @Override
    public Object visitFunctionCall(GrammarParser.FunctionCallContext ctx) {
        String funcName = ctx.IDENTIFIER().getText();


        if (funcName.equals("println")) {
            List<GrammarParser.ExprContext> expression = ctx.expr();

            List<Object> passedParameters = new LinkedList<>();
            for (int i=0; i<expression.size(); i++) {
                Object visited = super.visit(expression.get(i));
                passedParameters.add(visited);
            }

            if (passedParameters.size() == 1) {
                System.out.println(passedParameters.get(0).toString());
            } else {
                System.out.println(String.format(
                        passedParameters.get(0).toString(),
                        passedParameters.subList(1,passedParameters.size()).toArray()
                ));
            }
            return Void.TYPE;
        }

        Function function = functions.get(funcName);
        if (function == null) {
            throw new RuntimeException("No function with name " + funcName + " has been declared");
        }

        List<Function.Parameter> funcParameters = function.getParameters();
        List<GrammarParser.ExprContext> expression = ctx.expr();

        if (funcParameters.size() != expression.size()) {
            throw new RuntimeException("Wrong number of parameters for invocation function " + funcName);
        }

        List<Object> passedParameters = new LinkedList<>();
        for (int i=0; i<expression.size(); i++) {
            Object visited = super.visit(expression.get(i));
            Function.Parameter funcParam = funcParameters.get(i);


            if (visited.getClass().equals(Parameter.class)) {
                Object variableValue = variables.get(((Parameter) visited).getName());
                if (variableValue == null) {
                    throw new RuntimeException(String.format("Variable %s is not declared in scope", ((Parameter) visited).getName()));
                }
                passedParameters.add(variableValue);
            } else {
                if (    (Type.BOOL == funcParam.getType() && !visited.getClass().equals(Boolean.class))  ||
                        (Type.NUMBER == funcParam.getType() && !visited.getClass().equals(Double.class)) ||
                        (Type.STRING == funcParam.getType() && !visited.getClass().equals(String.class)) ||
                        (Type.CHAR == funcParam.getType() && !visited.getClass().equals(Character.class))) {

                    throw new RuntimeException(String.format("Wrong type of parameter on position %s, should be %s", i, funcParam.getType().getName()));
                }
                passedParameters.add(visited);
            }

        }

        HashMap<String, Object> funcInvocationVariables = new HashMap<>();
        for (int i=0; i<expression.size(); i++) {
            funcInvocationVariables.put(funcParameters.get(i).getName(), passedParameters.get(i));
        }

        InvocationVisitor invocationVisitor = new InvocationVisitor(functions, funcInvocationVariables, funcName);
        Object visitedFunctionBody = invocationVisitor.visitFunction_body(function.getBodyContext());
        return visitedFunctionBody;
    }

    @Override
    public Object visitStatement(GrammarParser.StatementContext ctx) {
        GrammarParser.Function_callContext function_callContext = ctx.function_call();
        return super.visitStatement(ctx);
    }

    @Override
    public Object visitFunction_body(GrammarParser.Function_bodyContext ctx) {
        List<ParseTree> children = ctx.children;

        //Object result = defaultResult();

        for (ParseTree child: children) {
            Object childResult = this.visit(child);
            if (childResult != null && childResult.getClass().equals(ReturnValue.class)) {
                return ((ReturnValue)childResult).getValue();
            }

            if (this.returnValue != null) {
                return this.returnValue.getValue();
            }
            //result = aggregateResult(result, childResult);
        }

        return new ReturnValue(null);
    }

    @Override
    public Object visitVariableDeclaration(GrammarParser.VariableDeclarationContext ctx) {

        String type = ctx.type().getText();
        Type typeEnum = Type.valueOf(type.toUpperCase());
        String name = ctx.IDENTIFIER().getText();
        Object variable = this.visit(ctx.expr());



        Class<?> varClass = variable.getClass();
        if (    (Type.BOOL == typeEnum && !varClass.equals(Boolean.class))  ||
                (Type.NUMBER == typeEnum && !varClass.equals(Double.class)) ||
                (Type.STRING == typeEnum && !varClass.equals(String.class)) ||
                (Type.CHAR == typeEnum && !varClass.equals(Character.class))) {

            throw new RuntimeException(String.format("Cannot create variable %s of type %s", name, type));
        }

        variables.put(name, variable);
        System.out.println("Declared variable: " + name + " = " + variable.toString());
        return variable;
    }

    @Override
    public Object visitStatementIf(GrammarParser.StatementIfContext ctx) {
        LogicalExpressionVisitor logicalExpressionVisitor = new LogicalExpressionVisitor(variables, this);
        Object result = null;
        if (logicalExpressionVisitor.visit(ctx.logical_expr())) {
            result = this.visit(ctx.function_body());
        } else {
            for (int i = 0; i < ctx.statement_else().size(); i++) {
                if (logicalExpressionVisitor.visit(ctx.statement_else(i).logical_expr())) {
                    result = this.visit(ctx.statement_else(i).function_body());
                    break;
                }
            }
        }
        return result;
    }


    @Override
    public Object visitStatementLoop(GrammarParser.StatementLoopContext ctx) {
        LogicalExpressionVisitor logicalExpressionVisitor = new LogicalExpressionVisitor(variables, this);
        Object result = null;
        while (logicalExpressionVisitor.visit(ctx.logical_expr())) {
            result = this.visit(ctx.function_body());
        }
        return result;
    }

    @Override
    public Object visitExpressionLogical(GrammarParser.ExpressionLogicalContext ctx) {
        LogicalExpressionVisitor logicalExpressionVisitor = new LogicalExpressionVisitor(variables, this);
        return logicalExpressionVisitor.visitExpressionLogical(ctx);
    }


    @Override
    public Object visitExpressionArithmetic(GrammarParser.ExpressionArithmeticContext ctx) {
        ArithmeticExpressionVisitor arithmeticExpressionVisitor = new ArithmeticExpressionVisitor(variables, this);
        return arithmeticExpressionVisitor.visitExpressionArithmetic(ctx);
    }


    @Override
    public Object visitStatementReturn(GrammarParser.StatementReturnContext ctx) {
        Object returnValue = this.visit(ctx.expr());
        Class<?> returnClass = returnValue.getClass();

        Type returnType = functions.get(this.functionName).getReturnType();


        if (    (Type.BOOL == returnType && !returnClass.equals(Boolean.class))  ||
                (Type.NUMBER == returnType && !returnClass.equals(Double.class)) ||
                (Type.STRING == returnType && !returnClass.equals(String.class)) ||
                (Type.CHAR == returnType && !returnClass.equals(Character.class))) {

            throw new RuntimeException(String.format("Return type for function %s should be %s", functionName, returnType.getName()));
        }

        this.returnValue = new ReturnValue(returnValue);
        return new ReturnValue(returnValue);
    }


    @Override
    public Object visitExpressionVarEntity(GrammarParser.ExpressionVarEntityContext ctx) {
        return super.visitExpressionVarEntity(ctx);
    }

    @Override
    public Object visitStringConst(GrammarParser.StringConstContext ctx) {
        String text = ctx.STRING().getText();
        return text.substring(1, text.length() - 1);
    }

    @Override
    public Object visitCharConst(GrammarParser.CharConstContext ctx) {
        return ctx.CHAR().getText().charAt(1);
    }


    @Override
    public Object visitNumericConst(GrammarParser.NumericConstContext ctx) {
        return Double.parseDouble(ctx.getText());
    }

    @Override
    public Object visitVariable(GrammarParser.VariableContext ctx) {
        String name = ctx.IDENTIFIER().getText();
        Object var = variables.get(name);
        if (var == null) {
            throw new RuntimeException(String.format("Problem with variable %s - might not exist or have wrong type", name));
        }
        return var;
    }

}


