package pl.edu.agh.visitors;

import pl.edu.agh.GrammarBaseVisitor;
import pl.edu.agh.GrammarParser;
import pl.edu.agh.enums.Type;
import pl.edu.agh.model.Function;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InterpreterVisitor extends GrammarBaseVisitor {
    public Map<String, Function> functions = new HashMap<>();

    @Override
    public Object visitFunction(GrammarParser.FunctionContext ctx) {

        GrammarParser.Function_declarationContext declaration = ctx.function_declaration();
        GrammarParser.Function_bodyContext body = ctx.function_body();

        String name = declaration.IDENTIFIER().getText();

        if (functions.containsKey(name)) {
            throw new RuntimeException("Function with this name is already defined");
        } else {
            List<Function.Parameter> parameters = declaration.function_param().stream()
                    .map(fp ->
                            new Function.Parameter(fp.IDENTIFIER().getText(),
                                    Type.valueOf(fp.type().getText().toUpperCase())))
                    .distinct()
                    .collect(Collectors.toList());

            if (declaration.function_param().size() != parameters.size()) {
                throw new RuntimeException("Parameters does not have distinct names");
            }

            Function func = Function.builder()
                    .name(name)
                    .parameters(parameters)
                    .returnType(Type.valueOf(declaration.return_type().getText().toUpperCase()))
                    .bodyContext(body)
                    .build();

            functions.put(name, func);

            if (name.equals("main")) {
                InvocationVisitor invocationVisitor = new InvocationVisitor(functions, new HashMap<>(), name);
                Object visit = invocationVisitor.visitFunction_body(func.getBodyContext());
                functions.remove(name);
                return visit;
            }

            System.out.println("Declared function: " + name);
            return func;
        }
    }
}