package pl.edu.agh.visitors;


import pl.edu.agh.GrammarBaseVisitor;
import pl.edu.agh.GrammarParser;

import java.util.Map;

public class ArithmeticExpressionVisitor extends GrammarBaseVisitor<Double> {


    private final Map<String, Object> variables;
    private final InvocationVisitor invocationVisitor;

    public ArithmeticExpressionVisitor(Map<String, Object> variables, InvocationVisitor invocationVisitor) {
        this.variables = variables;
        this.invocationVisitor = invocationVisitor;
    }


    @Override
    public Double visitExpressionArithmetic(GrammarParser.ExpressionArithmeticContext ctx) {
        return this.visit(ctx.arithmetic_expr());
    }


    @Override
    public Double visitArithmeticExpressionMult(GrammarParser.ArithmeticExpressionMultContext ctx) {
        return this.visit(ctx.arithmetic_expr(0)) * this.visit(ctx.arithmetic_expr(1));
    }

    @Override
    public Double visitArithmeticExpressionDiv(GrammarParser.ArithmeticExpressionDivContext ctx) {
        return this.visit(ctx.arithmetic_expr(0)) / this.visit(ctx.arithmetic_expr(1));
    }

    @Override
    public Double visitArithmeticExpressionMinus(GrammarParser.ArithmeticExpressionMinusContext ctx) {
        return this.visit(ctx.arithmetic_expr(0)) - this.visit(ctx.arithmetic_expr(1));

    }

    @Override
    public Double visitArithmeticExpressionPlus(GrammarParser.ArithmeticExpressionPlusContext ctx) {
        return this.visit(ctx.arithmetic_expr(0)) + this.visit(ctx.arithmetic_expr(1));
    }

    @Override
    public Double visitArithmeticExpressionNegation(GrammarParser.ArithmeticExpressionNegationContext ctx) {
        return -this.visit(ctx.arithmetic_expr());
    }

    @Override
    public Double visitArithmeticExpressionParens(GrammarParser.ArithmeticExpressionParensContext ctx) {
        return this.visit(ctx.arithmetic_expr());
    }

    @Override
    public Double visitArithmeticExpressionNumberEntity(GrammarParser.ArithmeticExpressionNumberEntityContext ctx) {
        return Double.parseDouble(ctx.getText());
    }

    @Override
    public Double visitArithmeticExpressionIdentifierEntity(GrammarParser.ArithmeticExpressionIdentifierEntityContext ctx) {
        String name = ctx.IDENTIFIER().getText();
        Object var = variables.get(name);
        if (var == null || !var.getClass().equals(Double.class)) {
            throw new RuntimeException(String.format("Problem with variable %s - might not exist or have wrong type", name));
        }
        return (Double) var;
    }

    @Override
    public Double visitArithmeticFunctionCall(GrammarParser.ArithmeticFunctionCallContext ctx) {
        return (Double) invocationVisitor.visitFunctionCall((GrammarParser.FunctionCallContext) ctx.function_call());
    }
}

