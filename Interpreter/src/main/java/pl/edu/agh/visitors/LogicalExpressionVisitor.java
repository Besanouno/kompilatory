package pl.edu.agh.visitors;


import pl.edu.agh.*;

import java.util.Map;

public class LogicalExpressionVisitor extends GrammarBaseVisitor<Boolean> {


    private final Map<String, Object> variables;
    private final InvocationVisitor invocationVisitor;

    public LogicalExpressionVisitor(Map<String, Object> variables, InvocationVisitor invocationVisitor) {
        this.variables = variables;
        this.invocationVisitor = invocationVisitor;
    }

    @Override
    public Boolean visitExpressionLogical(GrammarParser.ExpressionLogicalContext ctx) {
        return this.visit(ctx.logical_expr());
    }

    @Override
    public Boolean visitLogicalExpressionAnd(GrammarParser.LogicalExpressionAndContext ctx) {
        return this.visit(ctx.logical_expr(0)) && this.visit(ctx.logical_expr(1));
    }

    @Override
    public Boolean visitLogicalExpressionOr(GrammarParser.LogicalExpressionOrContext ctx) {
        return this.visit(ctx.logical_expr(0)) || this.visit(ctx.logical_expr(1));
    }

    @Override
    public Boolean visitComparisonExpression(GrammarParser.ComparisonExpressionContext ctx) {
        return super.visitComparisonExpression(ctx);
    }

    @Override
    public Boolean visitLogicalExpressionInParen(GrammarParser.LogicalExpressionInParenContext ctx) {
        return this.visit(ctx.logical_expr());
    }

    /*@Override
    public Boolean visitLogicalEntity(GrammarParser.LogicalEntityContext ctx) {
        return Boolean.valueOf(ctx.getText());
    }*/

    @Override
    public Boolean visitLogicalConst(GrammarParser.LogicalConstContext ctx) {
        return Boolean.valueOf(ctx.getText());
    }

    @Override
    public Boolean visitLogicalVariable(GrammarParser.LogicalVariableContext ctx) {
        String name = ctx.IDENTIFIER().getText();
        Object var = variables.get(name);
        if (var == null || !var.getClass().equals(Boolean.class)) {
            throw new RuntimeException(String.format("Problem with variable %s - might not exist or have wrong type", name));
        }
        return (Boolean) var;
    }

    @Override
    public Boolean visitComparisonExpressionEqual(GrammarParser.ComparisonExpressionEqualContext ctx) {
        ArithmeticExpressionVisitor arithmeticExpressionVisitor = new ArithmeticExpressionVisitor(variables, invocationVisitor);
        return arithmeticExpressionVisitor.visit(ctx.arithmetic_expr(0))
                .equals(arithmeticExpressionVisitor.visit(ctx.arithmetic_expr(1)));
    }

    @Override
    public Boolean visitComparisonExpressionGreaterEqual(GrammarParser.ComparisonExpressionGreaterEqualContext ctx) {
        ArithmeticExpressionVisitor arithmeticExpressionVisitor = new ArithmeticExpressionVisitor(variables, invocationVisitor);
        return arithmeticExpressionVisitor.visit(ctx.arithmetic_expr(0))
                >= arithmeticExpressionVisitor.visit(ctx.arithmetic_expr(1));
    }

    @Override
    public Boolean visitComparisonExpressionGreaterThan(GrammarParser.ComparisonExpressionGreaterThanContext ctx) {
        ArithmeticExpressionVisitor arithmeticExpressionVisitor = new ArithmeticExpressionVisitor(variables, invocationVisitor);
        return arithmeticExpressionVisitor.visit(ctx.arithmetic_expr(0))
                > arithmeticExpressionVisitor.visit(ctx.arithmetic_expr(1));
    }

    @Override
    public Boolean visitComparisonExpressionLowerEqual(GrammarParser.ComparisonExpressionLowerEqualContext ctx) {
        ArithmeticExpressionVisitor arithmeticExpressionVisitor = new ArithmeticExpressionVisitor(variables, invocationVisitor);
        return arithmeticExpressionVisitor.visit(ctx.arithmetic_expr(0))
                <= arithmeticExpressionVisitor.visit(ctx.arithmetic_expr(1));
    }

    @Override
    public Boolean visitComparisonExpressionLowerThan(GrammarParser.ComparisonExpressionLowerThanContext ctx) {
        ArithmeticExpressionVisitor arithmeticExpressionVisitor = new ArithmeticExpressionVisitor(variables, invocationVisitor);
        return arithmeticExpressionVisitor.visit(ctx.arithmetic_expr(0))
                < arithmeticExpressionVisitor.visit(ctx.arithmetic_expr(1));

    }

    @Override
    public Boolean visitComparisonExpressionNotEqual(GrammarParser.ComparisonExpressionNotEqualContext ctx) {
        ArithmeticExpressionVisitor arithmeticExpressionVisitor = new ArithmeticExpressionVisitor(variables, invocationVisitor);
        return !arithmeticExpressionVisitor.visit(ctx.arithmetic_expr(0))
                .equals(arithmeticExpressionVisitor.visit(ctx.arithmetic_expr(1)));
    }

    @Override
    public Boolean visitComparisonExpressionParens(GrammarParser.ComparisonExpressionParensContext ctx) {
        return this.visit(ctx.comparison_expr());
    }


    @Override
    public Boolean visitVariable(GrammarParser.VariableContext ctx) {
        String name = ctx.IDENTIFIER().getText();
        Object var = variables.get(name);
        if (var == null || !var.getClass().equals(Double.class)) {
            throw new RuntimeException(String.format("Problem with variable %s - might not exist or have wrong type", name));
        }
        return (Boolean) var;
    }

    @Override
    public Boolean visitLogicalExpressionFunctionCall(GrammarParser.LogicalExpressionFunctionCallContext ctx) {
        return (Boolean) invocationVisitor.visitFunctionCall((GrammarParser.FunctionCallContext) ctx.function_call());
    }
}