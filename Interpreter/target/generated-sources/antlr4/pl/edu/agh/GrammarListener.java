// Generated from pl/edu/agh/Grammar.g4 by ANTLR 4.7
package pl.edu.agh;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link GrammarParser}.
 */
public interface GrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link GrammarParser#rules}.
	 * @param ctx the parse tree
	 */
	void enterRules(GrammarParser.RulesContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#rules}.
	 * @param ctx the parse tree
	 */
	void exitRules(GrammarParser.RulesContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(GrammarParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(GrammarParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#function_declaration}.
	 * @param ctx the parse tree
	 */
	void enterFunction_declaration(GrammarParser.Function_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#function_declaration}.
	 * @param ctx the parse tree
	 */
	void exitFunction_declaration(GrammarParser.Function_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#function_param}.
	 * @param ctx the parse tree
	 */
	void enterFunction_param(GrammarParser.Function_paramContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#function_param}.
	 * @param ctx the parse tree
	 */
	void exitFunction_param(GrammarParser.Function_paramContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#function_body}.
	 * @param ctx the parse tree
	 */
	void enterFunction_body(GrammarParser.Function_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#function_body}.
	 * @param ctx the parse tree
	 */
	void exitFunction_body(GrammarParser.Function_bodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(GrammarParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(GrammarParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code FunctionCall}
	 * labeled alternative in {@link GrammarParser#function_call}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(GrammarParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code FunctionCall}
	 * labeled alternative in {@link GrammarParser#function_call}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(GrammarParser.FunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code StatementIf}
	 * labeled alternative in {@link GrammarParser#statement_if}.
	 * @param ctx the parse tree
	 */
	void enterStatementIf(GrammarParser.StatementIfContext ctx);
	/**
	 * Exit a parse tree produced by the {@code StatementIf}
	 * labeled alternative in {@link GrammarParser#statement_if}.
	 * @param ctx the parse tree
	 */
	void exitStatementIf(GrammarParser.StatementIfContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#statement_else}.
	 * @param ctx the parse tree
	 */
	void enterStatement_else(GrammarParser.Statement_elseContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#statement_else}.
	 * @param ctx the parse tree
	 */
	void exitStatement_else(GrammarParser.Statement_elseContext ctx);
	/**
	 * Enter a parse tree produced by the {@code StatementLoop}
	 * labeled alternative in {@link GrammarParser#statement_loop}.
	 * @param ctx the parse tree
	 */
	void enterStatementLoop(GrammarParser.StatementLoopContext ctx);
	/**
	 * Exit a parse tree produced by the {@code StatementLoop}
	 * labeled alternative in {@link GrammarParser#statement_loop}.
	 * @param ctx the parse tree
	 */
	void exitStatementLoop(GrammarParser.StatementLoopContext ctx);
	/**
	 * Enter a parse tree produced by the {@code StatementAssign}
	 * labeled alternative in {@link GrammarParser#statement_assign}.
	 * @param ctx the parse tree
	 */
	void enterStatementAssign(GrammarParser.StatementAssignContext ctx);
	/**
	 * Exit a parse tree produced by the {@code StatementAssign}
	 * labeled alternative in {@link GrammarParser#statement_assign}.
	 * @param ctx the parse tree
	 */
	void exitStatementAssign(GrammarParser.StatementAssignContext ctx);
	/**
	 * Enter a parse tree produced by the {@code StatementReturn}
	 * labeled alternative in {@link GrammarParser#statement_return}.
	 * @param ctx the parse tree
	 */
	void enterStatementReturn(GrammarParser.StatementReturnContext ctx);
	/**
	 * Exit a parse tree produced by the {@code StatementReturn}
	 * labeled alternative in {@link GrammarParser#statement_return}.
	 * @param ctx the parse tree
	 */
	void exitStatementReturn(GrammarParser.StatementReturnContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicalEntity}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 */
	void enterLogicalEntity(GrammarParser.LogicalEntityContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicalEntity}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 */
	void exitLogicalEntity(GrammarParser.LogicalEntityContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ComparisonExpression}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 */
	void enterComparisonExpression(GrammarParser.ComparisonExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ComparisonExpression}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 */
	void exitComparisonExpression(GrammarParser.ComparisonExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicalExpressionInParen}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 */
	void enterLogicalExpressionInParen(GrammarParser.LogicalExpressionInParenContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicalExpressionInParen}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 */
	void exitLogicalExpressionInParen(GrammarParser.LogicalExpressionInParenContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicalExpressionFunctionCall}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 */
	void enterLogicalExpressionFunctionCall(GrammarParser.LogicalExpressionFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicalExpressionFunctionCall}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 */
	void exitLogicalExpressionFunctionCall(GrammarParser.LogicalExpressionFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicalExpressionAnd}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 */
	void enterLogicalExpressionAnd(GrammarParser.LogicalExpressionAndContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicalExpressionAnd}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 */
	void exitLogicalExpressionAnd(GrammarParser.LogicalExpressionAndContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicalExpressionOr}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 */
	void enterLogicalExpressionOr(GrammarParser.LogicalExpressionOrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicalExpressionOr}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 */
	void exitLogicalExpressionOr(GrammarParser.LogicalExpressionOrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ComparisonExpressionEqual}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 */
	void enterComparisonExpressionEqual(GrammarParser.ComparisonExpressionEqualContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ComparisonExpressionEqual}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 */
	void exitComparisonExpressionEqual(GrammarParser.ComparisonExpressionEqualContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ComparisonExpressionNotEqual}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 */
	void enterComparisonExpressionNotEqual(GrammarParser.ComparisonExpressionNotEqualContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ComparisonExpressionNotEqual}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 */
	void exitComparisonExpressionNotEqual(GrammarParser.ComparisonExpressionNotEqualContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ComparisonExpressionGreaterThan}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 */
	void enterComparisonExpressionGreaterThan(GrammarParser.ComparisonExpressionGreaterThanContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ComparisonExpressionGreaterThan}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 */
	void exitComparisonExpressionGreaterThan(GrammarParser.ComparisonExpressionGreaterThanContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ComparisonExpressionGreaterEqual}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 */
	void enterComparisonExpressionGreaterEqual(GrammarParser.ComparisonExpressionGreaterEqualContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ComparisonExpressionGreaterEqual}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 */
	void exitComparisonExpressionGreaterEqual(GrammarParser.ComparisonExpressionGreaterEqualContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ComparisonExpressionLowerThan}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 */
	void enterComparisonExpressionLowerThan(GrammarParser.ComparisonExpressionLowerThanContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ComparisonExpressionLowerThan}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 */
	void exitComparisonExpressionLowerThan(GrammarParser.ComparisonExpressionLowerThanContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ComparisonExpressionLowerEqual}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 */
	void enterComparisonExpressionLowerEqual(GrammarParser.ComparisonExpressionLowerEqualContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ComparisonExpressionLowerEqual}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 */
	void exitComparisonExpressionLowerEqual(GrammarParser.ComparisonExpressionLowerEqualContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ComparisonExpressionVarEntity}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 */
	void enterComparisonExpressionVarEntity(GrammarParser.ComparisonExpressionVarEntityContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ComparisonExpressionVarEntity}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 */
	void exitComparisonExpressionVarEntity(GrammarParser.ComparisonExpressionVarEntityContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ComparisonExpressionParens}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 */
	void enterComparisonExpressionParens(GrammarParser.ComparisonExpressionParensContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ComparisonExpressionParens}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 */
	void exitComparisonExpressionParens(GrammarParser.ComparisonExpressionParensContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ArithmeticExpressionMult}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void enterArithmeticExpressionMult(GrammarParser.ArithmeticExpressionMultContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArithmeticExpressionMult}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void exitArithmeticExpressionMult(GrammarParser.ArithmeticExpressionMultContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ArithmeticExpressionMinus}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void enterArithmeticExpressionMinus(GrammarParser.ArithmeticExpressionMinusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArithmeticExpressionMinus}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void exitArithmeticExpressionMinus(GrammarParser.ArithmeticExpressionMinusContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ArithmeticFunctionCall}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void enterArithmeticFunctionCall(GrammarParser.ArithmeticFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArithmeticFunctionCall}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void exitArithmeticFunctionCall(GrammarParser.ArithmeticFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ArithmeticExpressionParens}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void enterArithmeticExpressionParens(GrammarParser.ArithmeticExpressionParensContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArithmeticExpressionParens}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void exitArithmeticExpressionParens(GrammarParser.ArithmeticExpressionParensContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ArithmeticExpressionDiv}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void enterArithmeticExpressionDiv(GrammarParser.ArithmeticExpressionDivContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArithmeticExpressionDiv}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void exitArithmeticExpressionDiv(GrammarParser.ArithmeticExpressionDivContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ArithmeticExpressionNumberEntity}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void enterArithmeticExpressionNumberEntity(GrammarParser.ArithmeticExpressionNumberEntityContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArithmeticExpressionNumberEntity}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void exitArithmeticExpressionNumberEntity(GrammarParser.ArithmeticExpressionNumberEntityContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ArithmeticExpressionPlus}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void enterArithmeticExpressionPlus(GrammarParser.ArithmeticExpressionPlusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArithmeticExpressionPlus}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void exitArithmeticExpressionPlus(GrammarParser.ArithmeticExpressionPlusContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ArithmeticExpressionNegation}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void enterArithmeticExpressionNegation(GrammarParser.ArithmeticExpressionNegationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArithmeticExpressionNegation}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void exitArithmeticExpressionNegation(GrammarParser.ArithmeticExpressionNegationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ArithmeticExpressionIdentifierEntity}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void enterArithmeticExpressionIdentifierEntity(GrammarParser.ArithmeticExpressionIdentifierEntityContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArithmeticExpressionIdentifierEntity}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 */
	void exitArithmeticExpressionIdentifierEntity(GrammarParser.ArithmeticExpressionIdentifierEntityContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicalConst}
	 * labeled alternative in {@link GrammarParser#logical_entity}.
	 * @param ctx the parse tree
	 */
	void enterLogicalConst(GrammarParser.LogicalConstContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicalConst}
	 * labeled alternative in {@link GrammarParser#logical_entity}.
	 * @param ctx the parse tree
	 */
	void exitLogicalConst(GrammarParser.LogicalConstContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicalVariable}
	 * labeled alternative in {@link GrammarParser#logical_entity}.
	 * @param ctx the parse tree
	 */
	void enterLogicalVariable(GrammarParser.LogicalVariableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicalVariable}
	 * labeled alternative in {@link GrammarParser#logical_entity}.
	 * @param ctx the parse tree
	 */
	void exitLogicalVariable(GrammarParser.LogicalVariableContext ctx);
	/**
	 * Enter a parse tree produced by the {@code NumericConst}
	 * labeled alternative in {@link GrammarParser#var_entity}.
	 * @param ctx the parse tree
	 */
	void enterNumericConst(GrammarParser.NumericConstContext ctx);
	/**
	 * Exit a parse tree produced by the {@code NumericConst}
	 * labeled alternative in {@link GrammarParser#var_entity}.
	 * @param ctx the parse tree
	 */
	void exitNumericConst(GrammarParser.NumericConstContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Variable}
	 * labeled alternative in {@link GrammarParser#var_entity}.
	 * @param ctx the parse tree
	 */
	void enterVariable(GrammarParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Variable}
	 * labeled alternative in {@link GrammarParser#var_entity}.
	 * @param ctx the parse tree
	 */
	void exitVariable(GrammarParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by the {@code StringConst}
	 * labeled alternative in {@link GrammarParser#var_entity}.
	 * @param ctx the parse tree
	 */
	void enterStringConst(GrammarParser.StringConstContext ctx);
	/**
	 * Exit a parse tree produced by the {@code StringConst}
	 * labeled alternative in {@link GrammarParser#var_entity}.
	 * @param ctx the parse tree
	 */
	void exitStringConst(GrammarParser.StringConstContext ctx);
	/**
	 * Enter a parse tree produced by the {@code CharConst}
	 * labeled alternative in {@link GrammarParser#var_entity}.
	 * @param ctx the parse tree
	 */
	void enterCharConst(GrammarParser.CharConstContext ctx);
	/**
	 * Exit a parse tree produced by the {@code CharConst}
	 * labeled alternative in {@link GrammarParser#var_entity}.
	 * @param ctx the parse tree
	 */
	void exitCharConst(GrammarParser.CharConstContext ctx);
	/**
	 * Enter a parse tree produced by the {@code VariableDeclaration}
	 * labeled alternative in {@link GrammarParser#var_declaration}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclaration(GrammarParser.VariableDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code VariableDeclaration}
	 * labeled alternative in {@link GrammarParser#var_declaration}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclaration(GrammarParser.VariableDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#return_type}.
	 * @param ctx the parse tree
	 */
	void enterReturn_type(GrammarParser.Return_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#return_type}.
	 * @param ctx the parse tree
	 */
	void exitReturn_type(GrammarParser.Return_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(GrammarParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(GrammarParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#number}.
	 * @param ctx the parse tree
	 */
	void enterNumber(GrammarParser.NumberContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#number}.
	 * @param ctx the parse tree
	 */
	void exitNumber(GrammarParser.NumberContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#character}.
	 * @param ctx the parse tree
	 */
	void enterCharacter(GrammarParser.CharacterContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#character}.
	 * @param ctx the parse tree
	 */
	void exitCharacter(GrammarParser.CharacterContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#string}.
	 * @param ctx the parse tree
	 */
	void enterString(GrammarParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#string}.
	 * @param ctx the parse tree
	 */
	void exitString(GrammarParser.StringContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#bool}.
	 * @param ctx the parse tree
	 */
	void enterBool(GrammarParser.BoolContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#bool}.
	 * @param ctx the parse tree
	 */
	void exitBool(GrammarParser.BoolContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExpressionVarEntity}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpressionVarEntity(GrammarParser.ExpressionVarEntityContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExpressionVarEntity}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpressionVarEntity(GrammarParser.ExpressionVarEntityContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExpressionFunctionCall}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpressionFunctionCall(GrammarParser.ExpressionFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExpressionFunctionCall}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpressionFunctionCall(GrammarParser.ExpressionFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExpressionArithmetic}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpressionArithmetic(GrammarParser.ExpressionArithmeticContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExpressionArithmetic}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpressionArithmetic(GrammarParser.ExpressionArithmeticContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExpressionLogical}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpressionLogical(GrammarParser.ExpressionLogicalContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExpressionLogical}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpressionLogical(GrammarParser.ExpressionLogicalContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExpressionLogicalEntity}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpressionLogicalEntity(GrammarParser.ExpressionLogicalEntityContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExpressionLogicalEntity}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpressionLogicalEntity(GrammarParser.ExpressionLogicalEntityContext ctx);
}