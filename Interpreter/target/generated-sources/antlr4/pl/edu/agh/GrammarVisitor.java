// Generated from pl/edu/agh/Grammar.g4 by ANTLR 4.7
package pl.edu.agh;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link GrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface GrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link GrammarParser#rules}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRules(GrammarParser.RulesContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(GrammarParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#function_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_declaration(GrammarParser.Function_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#function_param}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_param(GrammarParser.Function_paramContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#function_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_body(GrammarParser.Function_bodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(GrammarParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code FunctionCall}
	 * labeled alternative in {@link GrammarParser#function_call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionCall(GrammarParser.FunctionCallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StatementIf}
	 * labeled alternative in {@link GrammarParser#statement_if}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatementIf(GrammarParser.StatementIfContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#statement_else}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement_else(GrammarParser.Statement_elseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StatementLoop}
	 * labeled alternative in {@link GrammarParser#statement_loop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatementLoop(GrammarParser.StatementLoopContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StatementAssign}
	 * labeled alternative in {@link GrammarParser#statement_assign}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatementAssign(GrammarParser.StatementAssignContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StatementReturn}
	 * labeled alternative in {@link GrammarParser#statement_return}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatementReturn(GrammarParser.StatementReturnContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LogicalEntity}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalEntity(GrammarParser.LogicalEntityContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ComparisonExpression}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparisonExpression(GrammarParser.ComparisonExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LogicalExpressionInParen}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalExpressionInParen(GrammarParser.LogicalExpressionInParenContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LogicalExpressionFunctionCall}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalExpressionFunctionCall(GrammarParser.LogicalExpressionFunctionCallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LogicalExpressionAnd}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalExpressionAnd(GrammarParser.LogicalExpressionAndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LogicalExpressionOr}
	 * labeled alternative in {@link GrammarParser#logical_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalExpressionOr(GrammarParser.LogicalExpressionOrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ComparisonExpressionEqual}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparisonExpressionEqual(GrammarParser.ComparisonExpressionEqualContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ComparisonExpressionNotEqual}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparisonExpressionNotEqual(GrammarParser.ComparisonExpressionNotEqualContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ComparisonExpressionGreaterThan}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparisonExpressionGreaterThan(GrammarParser.ComparisonExpressionGreaterThanContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ComparisonExpressionGreaterEqual}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparisonExpressionGreaterEqual(GrammarParser.ComparisonExpressionGreaterEqualContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ComparisonExpressionLowerThan}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparisonExpressionLowerThan(GrammarParser.ComparisonExpressionLowerThanContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ComparisonExpressionLowerEqual}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparisonExpressionLowerEqual(GrammarParser.ComparisonExpressionLowerEqualContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ComparisonExpressionVarEntity}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparisonExpressionVarEntity(GrammarParser.ComparisonExpressionVarEntityContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ComparisonExpressionParens}
	 * labeled alternative in {@link GrammarParser#comparison_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparisonExpressionParens(GrammarParser.ComparisonExpressionParensContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ArithmeticExpressionMult}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArithmeticExpressionMult(GrammarParser.ArithmeticExpressionMultContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ArithmeticExpressionMinus}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArithmeticExpressionMinus(GrammarParser.ArithmeticExpressionMinusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ArithmeticFunctionCall}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArithmeticFunctionCall(GrammarParser.ArithmeticFunctionCallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ArithmeticExpressionParens}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArithmeticExpressionParens(GrammarParser.ArithmeticExpressionParensContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ArithmeticExpressionDiv}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArithmeticExpressionDiv(GrammarParser.ArithmeticExpressionDivContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ArithmeticExpressionNumberEntity}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArithmeticExpressionNumberEntity(GrammarParser.ArithmeticExpressionNumberEntityContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ArithmeticExpressionPlus}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArithmeticExpressionPlus(GrammarParser.ArithmeticExpressionPlusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ArithmeticExpressionNegation}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArithmeticExpressionNegation(GrammarParser.ArithmeticExpressionNegationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ArithmeticExpressionIdentifierEntity}
	 * labeled alternative in {@link GrammarParser#arithmetic_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArithmeticExpressionIdentifierEntity(GrammarParser.ArithmeticExpressionIdentifierEntityContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LogicalConst}
	 * labeled alternative in {@link GrammarParser#logical_entity}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalConst(GrammarParser.LogicalConstContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LogicalVariable}
	 * labeled alternative in {@link GrammarParser#logical_entity}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalVariable(GrammarParser.LogicalVariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code NumericConst}
	 * labeled alternative in {@link GrammarParser#var_entity}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumericConst(GrammarParser.NumericConstContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Variable}
	 * labeled alternative in {@link GrammarParser#var_entity}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(GrammarParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StringConst}
	 * labeled alternative in {@link GrammarParser#var_entity}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringConst(GrammarParser.StringConstContext ctx);
	/**
	 * Visit a parse tree produced by the {@code CharConst}
	 * labeled alternative in {@link GrammarParser#var_entity}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharConst(GrammarParser.CharConstContext ctx);
	/**
	 * Visit a parse tree produced by the {@code VariableDeclaration}
	 * labeled alternative in {@link GrammarParser#var_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclaration(GrammarParser.VariableDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#return_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn_type(GrammarParser.Return_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(GrammarParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#number}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumber(GrammarParser.NumberContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#character}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharacter(GrammarParser.CharacterContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#string}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString(GrammarParser.StringContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#bool}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBool(GrammarParser.BoolContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExpressionVarEntity}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionVarEntity(GrammarParser.ExpressionVarEntityContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExpressionFunctionCall}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionFunctionCall(GrammarParser.ExpressionFunctionCallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExpressionArithmetic}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionArithmetic(GrammarParser.ExpressionArithmeticContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExpressionLogical}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionLogical(GrammarParser.ExpressionLogicalContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExpressionLogicalEntity}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionLogicalEntity(GrammarParser.ExpressionLogicalEntityContext ctx);
}